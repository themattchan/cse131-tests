Test cases for cse131
=====================

## Running

Run from project dir: `~/path/to/compiler/`

`python tests/runTests.py <args>`

### Args

- `--gen` -- generates test answers from reference compiler

- `-v` or `--verbose` -- print failed test info

- `-vv` or `--all` -- print all test info

- `-d++<subdir>` -- run a subdirectory (arbitrary nesting from this dir, e.g. `-d++m` or `-d++a/b/c`)

## Credits

- My own: `m`

- Adapted from YUNOSOC (https://github.com/etinlb/YUNOSOC): `p, t`
    - Reference output was recompiled on ieng9 on October 20 2015

